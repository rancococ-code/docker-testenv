#!/bin/bash

set -e

chown -R nginx:nginx /data
chown -R nginx:nginx /data/www
chown -R nginx:nginx /data/tmp
chmod 755 /data/www
chown 755 /data/tmp

exec "$@"
